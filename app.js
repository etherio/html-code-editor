const app = {
    init() {
        this.theme.fetch();
        this.theme.el.addEventListener('change', (event) => {
            var el = document.querySelector('.CodeMirror');
            var name = event.target.value;
            var path = `./css/theme/${name}.css`;
            if (el instanceof HTMLElement) {
                el.className = `CodeMirror cm-s-${name}`;
            }
            this.theme.link.href = path;
            editor.options.theme = name;
        });
    },

    theme: {
        el: document.querySelector('.status-bar select.theme'),

        link: document.querySelector('link[role=theme]'),

        async fetch() {
            let response = await fetch('./themes.json');
            response = await response.json();
            response.forEach(theme => this.add(theme));
        },

        add({ name, path }) {
            var el = document.createElement('option');
            el.textContent = name;
            el.value = name;
            this.el.appendChild(el);
        },
    }
};


window.addEventListener('load', () => app.init());